<?php
require('init.php');
if ($connected === false) {
    header('Location: /../sessions/Views/connexion.php?redirect=nosession', true);
    exit();
}
$existinbucket = false;
foreach ($_POST as $key => $valeur) {
    if (!(array_key_exists($valeur, $_SESSION['Article'])))
        $_SESSION['Article'][$valeur] = $valeur; else
        $existinbucket = true;
}
$redirection = $_GET['from'];
if ($existinbucket === false) {
    header('Location: ../Views/' . $redirection . '.php?ajoutpanier=ok', true);
    exit();
} else {
    header('Location: ../Views/' . $redirection . '.php?ajoutpanier=okbut', true);
    exit();
}


