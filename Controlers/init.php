<?php
include("connexion_bdd/connexion.php");
var_dump($_COOKIE);
$connected = false;
$classac = "";
$classfi = "";
$classli = "";
$classal = "";
$classou = "";
$classpa = "";
$classco = "";
$pseudo = null;
$testvalue = null;
if (count($_COOKIE) !== 0) {
    foreach ($_COOKIE as $cle => $valeur) {
        if (preg_match('#session_#', $cle)) {
            $pseudo = $cle;
            $testvalue = $valeur;
        }
    }
}
if ($pseudo != null) {
    // session par cookies uniquement
    ini_set("session.use_cookies", 1);
    ini_set("session.use_only_cookies", 1);
    // validite : 5 minutes
    session_set_cookie_params(300);
    // modifie le nom du jeton par le u$ser
    session_name($pseudo);
    session_start();
    $connected = true;
    if ((session_id() === null)) {
        // supprimer le cookie de session
        setcookie(session_name(), '', time() - 300, "/");
        setcookie('test', '', time() - 300, "/");
        // supprimer les variables de session
        $_SESSION = array();
        // fermer la session
        session_destroy();
        $connected = false;
    }
    echo "Votre navigation est suivie par la session ";
    echo session_name(), " = ", session_id();
}
?>

