<?php
require('../init.php');
// supprimer le cookie de session
setcookie(session_name(), '', time() - 300, "/");
setcookie('test', '', time() - 300, "/");
// supprimer les variables de session
$_SESSION = array();
// fermer la session
if ($connected === true) {
    session_destroy();
    $connected = false;
}
header('Location: /../sessions/Views/connexion.php?redirect=deconnexion', true);
exit();
?>