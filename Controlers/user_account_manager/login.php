<?php
require("../connexion_bdd/connexion.php");
$cnx = connexion_bdd();
$pseudo = $_POST["pseudo"];
$mdp = $_POST["mdp"];
$req = "SELECT * FROM users WHERE pseudo=:pseudo AND mdp=:mdp";
$req_prep = $cnx->prepare($req);
$req_prep->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
$req_prep->bindParam(':mdp', $mdp, PDO::PARAM_STR);
$req_prep->execute();
$res = $req_prep->fetch();
$cnx = null;
if ($res === false) {
    echo "Erreur d'autentification";
} else {
    // session par cookies uniquement
    ini_set("session.use_cookies", 1);
    ini_set("session.use_only_cookies", 1);
    // validite : 5 minutes
    session_set_cookie_params(300);
    // modifie le nom du jeton par le user
    $pseudo = "session_" . $res['pseudo'];
    session_name($pseudo);
    setcookie("test", "preference", time() + 300, "/");
    session_start();
    $_SESSION['Article'] = Array();
}
header('Location: /../sessions/Views/connexion.php', true);
exit();
?>