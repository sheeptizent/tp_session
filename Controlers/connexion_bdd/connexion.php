<?php
require 'config_bdd.php';
/**
 * @return PDO Etablir une connexion
 */
function connexion_bdd()
{
    try {
        $cnx = new PDO(DSN, LOGIN, PASSWORD, OPTIONS);
        return $cnx;
    } catch (PDOException $e) {
        die("Echec de connexion à la base du site" . $e->getMessage());
    }
}

?>